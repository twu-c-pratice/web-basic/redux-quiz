1. 首页
   1. HomeHeader组件
   2. 首页Content
      1. 渲染单个post标题卡片(hard code)
      2. 渲染多个post标题卡片
         1. 使用Redux将获取到的服务器数据存储到state
         2. 利用state里的数据渲染多个卡片
   3. 笔记详情页
      1. 左侧笔记标题导航栏
      2. 右侧笔记正文
      3. 右侧底部按钮
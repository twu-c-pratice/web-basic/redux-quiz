import React, {Component} from 'react';
import '../style/PostTitleCard.less';

class NewPostCard extends Component {
  render() {
    return (
      <div className="post-title-card">
        <h3> {this.props.title} </h3>
      </div>
    );
  }
}

export default NewPostCard;
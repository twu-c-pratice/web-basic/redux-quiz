import React, {Component} from 'react';
import {MdEventNote} from 'react-icons/md';

import '../style/Header.less';
import {Link} from "react-router-dom";

class Header extends Component {
  render() {
    return (
      <header className="header">
        <Link to='/'><MdEventNote className="md-event-note"/></Link>
        <Link to='/'><h2 className="header-title">NOTES</h2></Link>
      </header>
    );
  }
}

export default Header;
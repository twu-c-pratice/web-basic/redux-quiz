import React, {Component} from 'react';
import '../style/PostTitleCard.less';
import {Link} from "react-router-dom";

class PostTitleCard extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="post-title-card">
        <Link to={`/notes/${this.props.id}`}>
          <h3> {this.props.title} </h3>
        </Link>
      </div>
    );
  }
}

export default PostTitleCard;
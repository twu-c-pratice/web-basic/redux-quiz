import React, {Component} from 'react';
import {connect} from "react-redux";
import '../style/Post.less'
import PostDetail from "./PostDetail";
import {bindActionCreators} from "redux";
import {getAllPosts} from "../actions/post";

class Post extends Component {

  constructor(props, context) {
    super(props, context);
    this.getPost = this.getPost.bind(this);
  }

  componentDidMount() {
    this.props.getAllPosts();
  }

  getPost() {
    const postId = parseInt(this.props.match.params.id);
    const posts = Array.from(this.props.posts);
    for (let post of posts) {
      if (post.id === postId) {
        return post;
      }
    }
  }

  render() {
    const post = this.getPost();
    return (
      <div className="post-page">
        <PostDetail post={post}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({posts: state.posts.posts});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllPosts: getAllPosts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Post);

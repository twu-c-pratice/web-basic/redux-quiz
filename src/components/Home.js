import React, {Component} from 'react';
import PostTitleCard from "./PostTitleCard";
import '../style/Home.less'
import {bindActionCreators} from "redux";
import {getAllPosts} from "../actions/post";
import {connect} from "react-redux";
import {MdLibraryAdd} from 'react-icons/md';
import NewPostCard from "./NewPostCard";


class Home extends Component {

  constructor(props, context) {
    super(props, context);
    this.getTitleCards = this.getTitleCards.bind(this);
  }

  componentDidMount() {
    this.props.getAllPosts();
  }

  getTitleCards() {
    const posts = Array.from(this.props.posts);
    return posts.map(post => <PostTitleCard key={post.id} title={post.title} id={post.id}/>);
  }

  render() {
    return (
      <div className="home">
        {this.getTitleCards()}
        <NewPostCard title={<MdLibraryAdd/>}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({posts: state.posts.posts});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllPosts: getAllPosts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
import React, {Component} from 'react';
import '../style/PostDetail.less'
import {bindActionCreators} from "redux";
import {getAllPosts} from "../actions/post";
import {connect} from "react-redux";


class PostDetail extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const post = this.props.post;
    return (
      <div className="post-detail">
        <h1>{post.title}</h1>
        <hr/>
        <article>{post.description}</article>
      </div>
    );
  }
}

const mapStateToProps = state => ({posts: state.posts.posts});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllPosts: getAllPosts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PostDetail);
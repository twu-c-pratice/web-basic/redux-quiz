import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from "./Home";
import "../style/Home.less";
import Post from "./Post";

const Content = () => (
  <main className="content">
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route exact path="/notes/:id" component={Post}/>
    </Switch>
  </main>
);

export default Content;
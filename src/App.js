import React, {Component} from 'react';
import './App.less';
import Header from "./components/Header";
import {BrowserRouter as Router} from 'react-router-dom';
import Content from "./components/Content";

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Router>
          <Header/>
          <Content/>
        </Router>
      </div>
    );
  }
}

export default App;
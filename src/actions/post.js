export const GET_ALL_NAME = 'GET_ALL_NAME';

function getAllPosts() {
  const url = "http://localhost:8080/api/posts";
  return (dispatch) => {
    fetch(url)
      .then(response => response.json())
      .then(result => {
        dispatch({
          type: GET_ALL_NAME,
          payload: result
        })
      })
  };
}

export {getAllPosts};

import {GET_ALL_NAME} from "../actions/post";

const initialState = {posts: {}};

export default function postReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_NAME :
      return {
        ...state,
        posts: action.payload
      };
    default:
      return state;
  }
}
